# react-native-grid-design

__Grid Design__ for __React-Native__, is a simple design tool which let you create your own grid system.
It consists on a view with a drawn customizable grid with a selection and snapping tools.

## It features
- A __customizable grid__ with allowing you to have any x and y numbers of cells
- An optional __select box__ which can be snapped to you grid cell
- An easy to use method letting you __snap anything__ to the grid
- __Customizable style__
- __Fast__ thanks to __ART__ module  

## How to install (NPM or Yarn)

```
npm install react-native-grid-design
```
or
```
yarn add react-native-grid-design
```

## Troubleshooting

If you meet any error with __ART__, be sure that the module is available. It should be by default if you use Expo.

Once your project is ejected, you need to link ART manually in your native project.

## How to use : Imports

{ __TouchEventType__ }: This enumeration allows you to know what type of touch happened on the grid. Possible values :
- PRESS_IN
- PRESSING
- PRESS_OUT

{ __GridCellType__ }: This enumeration allows you to define the type of the value you want to snap on the grid. Possible values :
- X
- Y

__Grid__ : ___Default element___ - This is the component you need to import to actually see the grid and the selector. Possible properties :

Features :
- __onTouchEvent__ : Callback - ___touchEventData___ parameter object
- __displaySelector__ : Boolean - Shows the selector on touch
- __gridCount__ : Object - __Required__ - Defines the number of cells for each columns and rows. Possible properties :
  - xCells: Number of columns
  - yCells: Number of rows
- __snapSelectionToGrid__ : Boolean - Snap the selector to the grid. Affects values of __onTouchEvent__
- __jailSelection__ : Boolean - When true, it prevents selector from getting out of bounds

Styling - Self explanatory:
- __gridCellsBorderColor__
- __gridCellsBorderWidth__
- __selectionBorderColor__
- __selectionBackgroundColor__

Container Styling :
- __style__

## How to use : Example

For more informations, simply look at the ___Example.js___ project which speaks for itself, in the _examples_ folder.

## How to snap my own elements

To snap any element to the grid, you need to create a ref of the grid.

Once you have one, simply use the __snapToGrid(value, GridCellType)__ method.
