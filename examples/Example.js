import React from 'react';
import { StyleSheet, View } from 'react-native';
import { GridType, TouchEventType } from 'react-native-grid-design';
import Grid from 'react-native-grid-design';

export default class App extends React.Component {

  onTouchEvent(touchEventData) {
    console.log(touchEventData)
  }

  render() {
    return (
      <Grid
        // Callback
        onTouchEvent={this.onTouchEvent.bind(this)}

        // Features
        displaySelector={true}
        gridCount={{ xCells: 8, yCells: 11 }}
        snapSelectionToGrid={true}
        jailSelection={false}

        // Styling
        gridCellsBorderColor={'#252525AA'}
        gridCellsBorderWidth={1}
        selectionBorderColor={'#353535'}
        selectionBackgroundColor={'#35353535'}

        // Global styling
        style={styles.container}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#111',
    borderWidth: 1,
    borderColor:'#222'
  }
});
